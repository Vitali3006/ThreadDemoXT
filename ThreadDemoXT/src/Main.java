import java.util.ArrayList;
import java.util.Deque;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.CopyOnWriteArrayList;

public class Main implements QuestionFeed.QuestionListener {
	private Deque<Question> questions = new ConcurrentLinkedDeque<>();
	private ArrayList<Question> answered = new ArrayList<>();
	private static Scanner sc = new Scanner(System.in);

	private Main() {
		QuestionFeed qf = QuestionFeed.getQuestionFeed();
		qf.setQuestionListener(this);
		while (true) {
			synchronized (this) {

				if (!questions.isEmpty()) {
					Question question = questions.pollFirst();
					System.out.println(questions.toString() + " there are "
							+ this.answered);
					String answer = sc.nextLine();
					question.setAnswerText(answer);
					this.answered.add(question);

				}
				try {
					wait();
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) {
		new Main();
		sc.close();
	}

	@Override
	public void process(Question q) {
		this.questions.addLast(q);
	}

}
