import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Question {
	private static DateFormat df = new SimpleDateFormat("HH:mm:ss");
	private String questionText;
	private String answerText;
	private long createdAt;
	private long answeredAt;

	public String getQuestionText() {
		return questionText;
	}

	public Question(String questionText) {
		this.questionText = questionText;
		this.createdAt = createdAt;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public long getAnsweredAt() {
		return answeredAt;
	}

	public void setAnsweredAt(long answeredAt) {
		this.answeredAt = answeredAt;
	}

	public String toString() {
		return df.format(new Date(createdAt) + " " + "questionText");
	}

}
