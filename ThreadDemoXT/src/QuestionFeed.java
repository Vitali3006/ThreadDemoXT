import java.util.Random;

public class QuestionFeed {
	private static QuestionFeed instance;
	private Thread myThread;
	private QuestionListener listener;

	private QuestionFeed() {
		this.myThread = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {

						Thread.sleep(10 + new Random().nextInt(9990));
					} catch (InterruptedException e) {
						Question q = new Question(
								"How are you?"
										+ ((1) + Math.random() * 100));
						if (listener != null) {
							listener.process(q);
							synchronized (listener) {
								listener.notify();
							}
						}
					}
					System.out.println(myThread.getName() + "finished");
				}
			}
		});
		this.myThread.setName("ThreadQuestionFeed");
		this.myThread.start();

	}

	public static QuestionFeed getQuestionFeed() {
		Class<QuestionFeed> clazz = QuestionFeed.class;
		synchronized (QuestionFeed.class) {

			if (instance == null) {
				instance = new QuestionFeed();
			}

			return instance;
		}
	}

	public interface QuestionListener {
		void process(Question question);
	}

	public QuestionListener getQuestionListener() {
		return listener;
	}

	public void setQuestionListener(QuestionListener QuestionListener) {
		this.listener = QuestionListener;
	}

}
